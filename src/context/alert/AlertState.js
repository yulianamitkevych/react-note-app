import React, {useReducer} from "react";
import {AlertContext} from "./alertContext";
import {AlertReducer} from "./alertReducer";
import {HIDE_ALERT, SHOW_ALERT} from "../types";

export const AlertState = ({children}) => {
    const [state, dispatch] = useReducer(AlertReducer, {visible: false});

    const Show = (text, type: 'warning') => {
        dispatch({
            type: SHOW_ALERT,
            payload: {text, type}
        });

        setTimeout(()=>{dispatch({type: HIDE_ALERT})}, 2500);
    }

    const Hide = () => dispatch({type: HIDE_ALERT});

    return (
        <AlertContext.Provider value={{
            Show, Hide,
            alert: state
        }}>
            {children}
        </AlertContext.Provider>
    )
}