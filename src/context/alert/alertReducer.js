import {HIDE_ALERT, SHOW_ALERT} from "../types";

const Handlers = {
    [SHOW_ALERT]: (state, {payload}) => ({...payload, visible: true}),
    [HIDE_ALERT]: state => ({...state, visible: false}),
    DEFAULT: state => state
}

export const AlertReducer = (state, action) => {
    const Handle = Handlers[action.type] || Handlers.DEFAULT
    return Handle(state, action);
}