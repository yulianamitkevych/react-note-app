import {ADD_NOTE, FETCH_NOTES, REMOVE_NOTE, SHOW_LOADER} from "../types";

const Handlers = {
    [SHOW_LOADER]: (state) => ({...state, loading: true}),
    [ADD_NOTE]: (state, {payload}) => ({
        ...state,
        notes: [...state.notes, payload]
    }),
    [FETCH_NOTES]: (state, {payload}) => ({...state, notes: payload, loading: false}),
    [REMOVE_NOTE]: (state, {payload}) => ({
       ...state,
       notes: state.notes.filter(note => note.id !== payload)
    }),
    DEFAULT: state => state
}

export const DatabaseReduser = (state, action) => {
    const Handle = Handlers[action.type] || Handlers.DEFAULT;
    return Handle(state, action);
}