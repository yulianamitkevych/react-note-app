import React, {useReducer} from "react";
import axios from "axios";
import {DatabaseContext} from "./databaseContext";
import {DatabaseReduser} from "./databaseReduser";
import {ADD_NOTE, FETCH_NOTES, REMOVE_NOTE, SHOW_LOADER} from "../types";

const url = process.env.REACT_APP_DB_URL;

export const DatabaseState = ({children}) => {
    const InitialState = {
        notes: [],
        loading: false
    };
    const [state, dispatch] = useReducer(DatabaseReduser, InitialState);

    const ShowLoader = () => dispatch({type: SHOW_LOADER});

    const FetchNotes = async () => {
        ShowLoader();
        const res = await axios.get(`${url}/notes.json`);

       const payload = Object.keys(res.data).map(key => {
            return {
                ...res.data[key],
                id: key
            }
        });

       dispatch({
           type: FETCH_NOTES,
           payload
       })
    }

    const AddNote = async title => {
        const note = {
            title, data: new Date().toJSON()
        }

        try {
            const res = await axios.post(`${url}/notes.json`, note);
            const payload = {
                ...note,
                id: res.data.name
            };

            dispatch({
                type: ADD_NOTE,
                payload
            });
        } catch (e) {
            throw new Error(e.message);
        }
    }

    const RemoveNote = async id => {
        await axios.delete(`${url}/notes/${id}.json`);

        dispatch({
            type: REMOVE_NOTE,
            payload: id
        })
    }

    return (
        <DatabaseContext.Provider value={{
            ShowLoader, AddNote, FetchNotes, RemoveNote,
            loading: state.loading,
            notes: state.notes
        }}>
            {children}
        </DatabaseContext.Provider>
    )
}