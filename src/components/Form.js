import React, {useState, useContext} from 'react';
import {AlertContext} from "../context/alert/alertContext";
import {DatabaseContext} from "../context/datbase/databaseContext";

export const Form = () => {
    const [Value, setValue] = useState('');
    const alert = useContext(AlertContext);
    const database = useContext(DatabaseContext);

    const submitHandler = event => {
        event.preventDefault();

        if (Value.trim()) {
            database.AddNote(Value.trim()).then(() => {
                alert.Show('The note was added', 'success');
            }).catch(() => {
                alert.Show('Ooops, something wrong', 'danger');
            });
            setValue('');
        } else {
            alert.Show('Enter a name, please', 'danger');
        }
    }

    return (
        <form onSubmit={submitHandler}>
            <div className="form-group">
                <input
                    type="text"
                    className="form-control"
                    placeholder="Enter a name of your note"
                    value={Value}
                    onChange={e => setValue(e.target.value)}/>
            </div>
        </form>
    )
}