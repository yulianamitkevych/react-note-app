import React, {useContext} from 'react';
import {CSSTransition} from 'react-transition-group';
import {AlertContext} from "../context/alert/alertContext";

export const Alert = () => {
    const {alert, Hide} = useContext(AlertContext);

    return (
        <CSSTransition
        in={alert.visible}
        timeout={{
            enter: 500,
            exit: 350
        }}
        classNames={'alert'}
        mountOnEnter
        unmountOnExit
        >
            <div className={`alert alert-${alert.type || 'warning'} alert-dismissible fade show`}>
                <strong>Attention! </strong>
                {alert.text}
                <button onClick={Hide} type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close"/>
            </div>
        </CSSTransition>
    )
}