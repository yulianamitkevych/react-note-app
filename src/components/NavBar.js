import React from 'react';
import {NavLink} from 'react-router-dom';

export const NavBar = () => (
    <nav className={'navbar navbar-dark navbar-expand-lg bg-info px-3'}>
        <div className="container-fluid">
            <div className="navbar-brand">
                <h3>Note App</h3>
            </div>
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <NavLink
                            className="nav-link"
                            to="/"
                            exact
                        >Home
                        </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink
                            className="nav-link"
                            to="/about"
                        >About
                        </NavLink>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
)
