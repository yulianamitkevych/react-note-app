import React, {useContext} from 'react';
import {CSSTransition, TransitionGroup} from "react-transition-group";
import {AlertContext} from "../context/alert/alertContext";

export const Notes = ({notes, onRemove}) => {
    const alert = useContext(AlertContext);

    return (
        <TransitionGroup component='ul' className='list-group'>
            {notes.map(note => (
                <CSSTransition
                    key={note.id}
                    classNames={'note'}
                    timeout={800}
                >
                    <li className="list-group-item list-group-item-action border border-info m-1 note">
                        <strong>{note.title}</strong>
                        <span>{`${note.data.slice(0, 10)} ${note.data.slice(11, 16)}`}</span>
                        <button
                            type="button"
                            className="btn btn-outline-danger btn-sm li-btn"
                            onClick={() => {
                                alert.Show('The note was deleted', 'success');
                                onRemove(note.id);
                            }}
                        >
                            &times;
                        </button>
                    </li>
                </CSSTransition>
            ))}
        </TransitionGroup>
    )
}