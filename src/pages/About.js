import React from 'react';

export const About = () => (
    <div className="jumbotron jumbotron-fluid">
        <div className="container">
            <h1 className="display-4">Notes React application</h1>
            <p className="lead">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci amet asperiores culpa cumque eos
                expedita hic illo in iure necessitatibus omnis quam quia, quibusdam reprehenderit sit totam ut vero
                vitae.
            </p>
        </div>
    </div>
)