import React, {Fragment, useContext, useEffect} from 'react';
import {Form} from '../components/Form';
import {Notes} from "../components/Notes";
import {DatabaseContext} from "../context/datbase/databaseContext";
import {Loader} from "../components/Loader";

export const Home = () => {
    const {loading, notes, FetchNotes, RemoveNote} = useContext(DatabaseContext);

    useEffect(() => {
        FetchNotes();
        // eslint-disable-next-line
    }, []);

    return (
        <Fragment>
            <Form/>
            <hr/>
            {
                loading
                    ? <Loader/>
                    : <Notes notes={notes} onRemove={RemoveNote}/>

            }
        </Fragment>
    )
}